import {LitElement, html} from 'lit-element'

class MetaForm extends LitElement {
    static get properties() {
        return {
            goal: {type: Object}
        }
    }


    constructor() {
        super();
        this.goal = {};

    }


    render() {
        /*esto es el template*/
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        
        <link rel="stylesheet" href="./css/metas-styles.css">
        <h4>Crear una meta nueva!</h4>
        <br/>
        <div>
            <form>
                <div class="row">
                    <div class="col">
                        <span>Qué te gustaría conseguir?</span>
                        <br/>
                        <input type="text" class="form-control" 
                            placeholder="Nombre" 
                            @input="${this.updateName}"  />
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span>Cuánto dinero requiere tu meta?</span>
                        <br/>
                        <input type="text" class="form-control" 
                            placeholder="Importe Objetivo" 
                            @input="${this.updateImporte}"  />
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span>Cuándo te propones conseguirlo?</span>
                        <br/>                    
                        <input type="date" class="form-control" 
                            placeholder="Fecha Fin" 
                            @input="${this.updateFechaFin}"  />
                    </div>
                </div>
                <br/>
                <br/>
                <div style="float: right;">
                    <button class="btn-secondary" @click="${this.goBack}"><strong>Back</strong></button>
                    <button class="btn-success" @click="${this.storeNewGoal}"><strong>Guardar</strong></button>                
                </div>

                <br/>
                <br/>
        </div>

        `;
    }


    updateName(e)
    {
        this.goal.nombre = e.target.value;
    }

    updateImporte(e)
    {
        this.goal.importe_objetivo = e.target.value;
    }

    updateFechaFin(e)
    {
        this.goal.fecha_fin = e.target.value;
    }


    storeNewGoal(e)
    {
        console.log("metas-form.js ::: storeNewGoal");
        e.preventDefault();

        this.goal.photo = {
            src: "./img/usuario.jpg",
            alt: "Persona"
        }

        this.dispatchEvent(new CustomEvent("meta-form-store", {
            detail: {
                goal: {
                    nombre: this.goal.nombre,
                    importe_objetivo: this.goal.importe_objetivo,
                    fecha_fin: this.goal.fecha_fin,
                    importe_conseguido: 0,
                    photo: this.goal.photo
                }
            }
        }));


    }    

}


/*aqui se declara el custom element*/
customElements.define('metas-form', MetaForm)