import { LitElement, html } from 'lit-element';

class MetaFichaListado extends LitElement {

    static get properties() {
        return {
          id : {type: String},
          nombre : {type: String},
          fecha_alta: {type: Date},
          fecha_fin: {type: Date},
          importe_objetivo: {type: Number},
          importe_conseguido: {type: Number},
          cliente: {type: String},
          cuenta_meta: {type: String},
          status: {type: String},
          photo: {type: Object},
          progreso: {type: Number},
        };
    }

    constructor() {
        super();

    }

    render() {
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">        
        <link rel="stylesheet" href="./css/metas-styles.css">
        
        <div class="card h-100">
                <img src="${this.photo.src}" alt="${this.photo.alt}"  class="card-img-top"/>
                <div class="card-body">

                    <h6 class="card-title"><b>Objetivo:</b> ${this.nombre}</h6>
                    <h6 class="card-title"><b>Fecha fin:</b> ${this.fecha_fin}</h6>
                    <h6 class="card-title"><b>Objetivo:</b> ${this.importe_objetivo}</h6>
                    <h6 class="card-title"><b>Conseguido:</b> ${this.importe_conseguido}</h6>

                </div>

                <div class="card-footer">
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: ${this.progreso}%;" aria-valuenow="${this.progreso}" aria-valuemin="0" aria-valuemax="100">${this.progreso}%</div>

                    </div>
                </div>
                <div class="card-footer">
                    <button @click="${this.aportar50}" class="btn btn-secondary col-5 "><strong>-50</strong></button>
                    <button @click="${this.aportar200}" class="btn btn-info col-5 offset-1"><strong>+200</strong></button>            
                </div>	                
            </div>
        `;
    }

    updated(changedProperties) {
        //TBD  

    
    }


    miProgreso()
    {
        return "25";//this.importe_conseguido/this.importe_objetivo;
    }

    aportar(valor)
    {
        this.dispatchEvent(
            new CustomEvent(
                "aportar-cash",
                {
                    detail: {
                        nombre: this.nombre,
                        cash: valor
                    }
                }
            )
        );          
    }
    aportar200()
    {
        console.log("aportar200")
        this.aportar(200);

      
    }

    aportar50()
    {
        console.log("aportar50")
        this.aportar(-50);
    }    

}

customElements.define('meta-ficha-listado', MetaFichaListado)
