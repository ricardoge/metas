import {LitElement, html} from 'lit-element'
import '../metas-main/metas-main.js'; 
import '../metas-header/metas-header.js';
import '../metas-footer/metas-footer.js'; 
import '../metas-sidebar/metas-sidebar.js'; 




class MetasApp extends LitElement {

    static get properties() {
        return {			
            metas: {type: Array}
        };
    }	

    render() {

        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        
        <link rel="stylesheet" href="./css/metas-styles.css">

        <metas-header></metas-header>

        <div class="container" style="margin-top:30px">
            <div class="row">
                <div class="col-sm-2 sidenav">
                    <metas-sidebar @create-meta="${this.createNewMeta}"></metas-sidebar>     
                </div>
                <div class="col-sm-10 text-left mainvw"> 
                    <metas-main></metas-main>

                </div>
            </div>        
        </div>
        


        <metas-footer></metas-footer>



            
        `;
    }

    createNewMeta(e) {
        console.log("metas-app.js ::: createNewMeta");    
        this.shadowRoot.querySelector("metas-main").showNewElementForm = true;    
    }

    updated(changedProperties){
        console.log("metas-app.js ::: updated");

        if(changedProperties.has("metas"))
        {
            console.log("metas-app.js ::: metas updated");
        }
    }
    
    
}

customElements.define('metas-app', MetasApp);
