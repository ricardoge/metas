import {LitElement, html} from 'lit-element'




class MetasFooter extends LitElement {


    render() {

        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <link rel="stylesheet" href="./css/metas-styles.css">

        
        <div class="jumbotron text-center" style="margin-bottom:100">
            <p>Practitioner 2.0 Hackathon ::: Noviembre 2020</p>
        </div>
            
        `;
    }


    
    
}

customElements.define('metas-footer', MetasFooter);
