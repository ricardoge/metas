import {LitElement, html} from 'lit-element'
import '../metas-main-dm/metas-main-dm.js';
import '../metas-ficha-listado/metas-ficha-listado.js';
import '../metas-ficha-listado/metas-ficha-listado.js';
import '../metas-form/metas-form.js';

class MetasMain extends LitElement {

    static get properties() {
        return {
            metas: {type: Array},
            showNewElementForm: {type: Boolean}
        };
    }

    constructor() {
        super();
        this.metas = [];
        this.showNewElementForm = false;

    }
    render() {
        /*esto es el template*/
        console.log("metas-main.js ::: render" + this.metas);
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        
        <link rel="stylesheet" href="./css/metas-styles.css">

        <h1>Mis Metas</h1>
        <hr>
        <div id="listaMetas">
            <div class="row row-cols-1 row-cols-sm-4">
                ${this.metas.map(
                    meta =>
                        html`<meta-ficha-listado
                                nombre="${meta.nombre}"
                                fecha_fin="${meta.fecha_fin}"
                                importe_objetivo="${meta.importe_objetivo}"
                                importe_conseguido="${meta.importe_conseguido}"
                                progreso="${meta.progreso}"
                                .photo="${meta.photo}"
                                @aportar-cash="${this.aportarCash}">
                        </meta-ficha-listado>`
                    )
                }
            </div>

        </div>
        <div >
            <metas-form id="formMetas" class="d-none" @meta-form-store="${this.storeNewGoal}"></metas-form> 
        </div>        
         

        <metas-main-dm @updated-metas-dm="${this.updatedMetasFromDM}"></metas-main-dm>
        `;
    }

    updatedMetasFromDM(e) {
        console.log("metas-main.js ::: updatedMetasFromDM");
        this.metas = e.detail.metas;
        
        for (var i = 0; i < this.metas.length; i++) {
            this.metas[i].progreso = 100*(this.metas[i].importe_conseguido/this.metas[i].importe_objetivo)
            
            console.log("progreso: " + this.metas[i].progreso)            
        }
       
    }

    aportarCash(e) {
        console.log("metas-main.js ::: aportarCash");   
        
        for (var i = 0; i < this.metas.length; i++) {
            if(this.metas[i].nombre === e.detail.nombre)
            {
                
                this.metas[i].importe_conseguido = Math.max(0,Math.min(this.metas[i].importe_conseguido + e.detail.cash, this.metas[i].importe_objetivo));  

                let nuevoProgreso = Math.round(100*(this.metas[i].importe_conseguido/this.metas[i].importe_objetivo));
                this.metas[i].progreso= nuevoProgreso>100?100:nuevoProgreso;
                
                //update with spread syntax
                this.metas = [...this.metas];
            }
        }        
  
    }

    storeNewGoal(e) {
        console.log("metas-main.js ::: storeNewGoal");
        console.log(e.detail.goal);
        e.detail.goal.progreso=0;
        this.metas = [...this.metas, e.detail.goal];//spread syntax    
        this.showNewElementForm = false;        
    }

    updated(changedProperties) {
        console.log("metas-main.js ::: Updated()");

        if(changedProperties.has("metas")) {
            console.log("metas-main.js ::: Propiedad metas updated");
            this.dispatchEvent(
                new CustomEvent(
                    "updated-metas",
                    {
                        detail: {
                            metas: this.metas
                        }
                    }
                )
            );
        }

        if(changedProperties.has("showNewElementForm")) {
            console.log("metas-main.js ::: Propiedad showNewElementForm");
            if(this.showNewElementForm == true)
            {
                this.showMetasFormData();

            }else
            {
                this.showMetasList();
            }            
        }        
    }

    showMetasFormData()
    {
        console.log("metas-main.js ::: Propiedad showMetasFormData");
        console.log(this.shadowRoot.getElementById("listaMetas"))
        this.shadowRoot.getElementById("listaMetas").classList.add("d-none");
        this.shadowRoot.getElementById("formMetas").classList.remove("d-none");
    }
    showMetasList()
    {
        console.log("metas-main.js ::: Propiedad showMetasList");
        this.shadowRoot.getElementById("listaMetas").classList.remove("d-none");
        this.shadowRoot.getElementById("formMetas").classList.add("d-none");
    }

}


/*aqui se declara el custom element*/
customElements.define('metas-main', MetasMain)
