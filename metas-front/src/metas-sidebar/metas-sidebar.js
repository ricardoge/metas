import {LitElement, html} from 'lit-element'




class MetasSidebar extends LitElement {


    render() {

        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        
        <link rel="stylesheet" href="./css/metas-styles.css">

        <br/><br/>

        <button class="btn btn-primary" @click="${this.addNuevaMeta}">Nueva Meta +</button>	




      </div>        
        `;
    }

    addNuevaMeta(e){
        console.log("metas-sidebar.js ::: addNuaveMeta")
        e.preventDefault();    
        this.dispatchEvent(new CustomEvent("create-meta", {}));
    }

    
    
}

customElements.define('metas-sidebar', MetasSidebar);
