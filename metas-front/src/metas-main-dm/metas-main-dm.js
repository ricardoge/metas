import {LitElement, html} from 'lit-element'

class MetasMainDM extends LitElement {

    static get properties() {
        return {
            metas: {type: Array},   
            metasBack: {type: Array},   
            contadorLlamadas: {type: Number}    
        };
    }

    constructor() {
        super();
        console.log("metas-main-dm.js ::: Constructor()");

        this.contadorLlamadas=0;

        let xhr = new XMLHttpRequest();
        this.contadorLlamadas=this.contadorLlamadas+1;
        //open prepara la peti, configuracion, etc
        xhr.open("GET", "http://localhost:8081/apitechu/v1/meta");
        xhr.send(/*aqui se envia el body*/);
        console.log("Fin de llamada a metas")        
        
        xhr.onload = () => {
            if(xhr.status===200)
            {
                console.log("HTTP200 - Peticion completada");                
                //console.log(xhr.responseText);
                let APIResponse = JSON.parse(xhr.responseText);                
                this.metasBack=APIResponse;//(sin results)
                this.metasBack.map( 
                    meta => meta.photo= {
                        "src": "./img/usuario.jpg",
                        "alt": "meta"
                    }
                );    
                console.log(this.metasBack);
                this.metas=[];//go to updated              
            }
        };
        
       
    }

    updated(changedProperties)
    {
        console.log("metas-main-dm.js ::: Updated()");

       
    
        if(changedProperties.has("metas")) {
            console.log("metas-main-dm.js ::: Propiedad metas updated");
            this.dispatchEvent(
                new CustomEvent(
                    "updated-metas-dm",
                    {
                        detail: {
                            metas: this.metasBack
                        }
                    }
                )
            );
        }
        

    }

}


/*aqui se declara el custom element*/
customElements.define('metas-main-dm', MetasMainDM)
